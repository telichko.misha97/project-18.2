﻿#include <iostream>
#include <stack>
#include <cassert>
#include <string>
#include <iomanip>


template <typename T>

class stack
{
private:
    T* stackPtr; // указатель 
    int size; // размер 
    int top; // номер стека 
public:
    stack(int = 10);
    ~stack();


    inline void push(const T&); // помещаем элемент в вершину 
    inline T pop(); // удаляем верхний элемент 
    inline void printStack(); // выводим на экран 
};

    template <typename T>
    stack<T>::stack(int maxSize) :
        size(maxSize)
    {
        stackPtr = new T[size]; // выделяем память 
        top = 0; // новый элемнт инициализируем как 0
    }

   

    // деструктор стека
    template <typename T>
    stack<T>::~stack()
    {
        delete[] stackPtr; 
    }
    // добавляем элемент
    template<typename T>
    inline void stack<T>::push(const T & value)
    {
        assert(top < size);
        stackPtr[top++] = value;
    }
    
    template <typename T>
    inline T stack<T>::pop()
    {
        assert(top > 0);
       return stackPtr[--top];
    }
        // вывод в консоль
        template<typename T>
    inline void stack<T>::printStack()
    {
        for (int i = top - 1; i >= 0; i--)
            std::cout << " " << std::setw(2) << stackPtr[i];
    }

    int main()
{
        stack<char> stackSymvol(5);
        int ct = 0;
        char ch;
        while (ct++ < 5)
        {
            std::cin >> ch;
            stackSymvol.push(ch); // заполняем стек
        } 

        std::cout << std::endl;

        stackSymvol.printStack(); // печать полного стека
        
        std::cout << "\n pop  ";
        stackSymvol.pop(); // удаление улемента
        stackSymvol.printStack(); // вывод в консоль без элемента

}